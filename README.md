# Propeller Junior Frontend Assignment

This project is a simple test e-commerce web application built with Angular, utilizing the GraphQL API provided
at [demo.vendure.io/shop-api](https://demo.vendure.io/shop-api).

## Getting Started

To get started with this project, follow these steps:

1. Clone this repository to your local machine.
   `git clone https://gitlab.com/kiko.milenkovski10/e-commerce`

2. Navigate to the project directory.

3. Install dependencies.
   `npm install`

## Running the Application

Run the following command to start the development server and view the application in your web browser.
`ng serve --open`

## Features

- Product listings page with sorting and filtering.
- Product detail page with additional information.
- Button on the product detail page to add the product to the active order.
- Page to view the details of the order.
- Visually appealing styling using Angular Materials and Tailwind CSS.

## Folder Structure

The project follows a standard Angular project structure. Key folders and files include:

    src/app/components: Angular components
    src/app/modules: Angular pages
    src/app/services: Services used for API communication
    ...

## Testing

Unit tests have been implemented to ensure the reliability of the application.

Run the following command to execute the tests.
`ng test`

## Technologies Used

    Angular
    GraphQL
    Angular Materials
    NgRx for state management
    Tailwind CSS


