import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductListingComponent} from './product-listing.component';
import {Apollo} from "apollo-angular";
import {
  ActionsSubject,
  ReducerManager,
  ReducerManagerDispatcher,
  StateObservable,
  Store,
  StoreModule
} from "@ngrx/store";
import {DropdownModule} from "../../components/dropdown/dropdown.module";
import {SearchModule} from "../../components/search/search.module";
import {ButtonModule} from "../../components/button/button.module";

describe('ProductListingComponent', () => {
  let component: ProductListingComponent;
  let fixture: ComponentFixture<ProductListingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductListingComponent],
      providers: [Apollo, Store, StateObservable, ActionsSubject, ReducerManager, ReducerManagerDispatcher],
      imports: [StoreModule.forRoot({}), DropdownModule, SearchModule, ButtonModule]
    });
    fixture = TestBed.createComponent(ProductListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
