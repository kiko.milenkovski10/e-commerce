import { createAction, props } from '@ngrx/store';

export const setDropdownOption = createAction(
  '[Dropdown] Set Dropdown Option',
  props<{ dropdownOption: string }>()
);
