import { createReducer, on } from '@ngrx/store';
import * as DropdownActions from '../actions/dropdown.actions';

export interface DropdownState {
  dropdownOption: string;
}

export const initialState: DropdownState = {
  dropdownOption: 'ASC',
};

export const dropdownReducer = createReducer(
  initialState,
  on(DropdownActions.setDropdownOption, (state, { dropdownOption }) => ({
    ...state,
    dropdownOption,
  }))
);
