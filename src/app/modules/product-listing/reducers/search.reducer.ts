import {createReducer, on} from '@ngrx/store';
import * as SearchActions from '../actions/search.actions';

export interface SearchState {
  searchValue: string;
}

export const initialState: SearchState = {
  searchValue: '',
};

export const searchReducer = createReducer(
  initialState,
  on(SearchActions.setSearchValue, (state, {searchValue}) => ({
    ...state,
    searchValue,
  }))
);
