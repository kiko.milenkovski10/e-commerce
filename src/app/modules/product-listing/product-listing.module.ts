import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProductListingComponent} from "./product-listing.component";
import {CardModule} from "../../components/card/card.module";
import {DropdownModule} from "../../components/dropdown/dropdown.module";
import {SearchModule} from "../../components/search/search.module";
import {ButtonModule} from "../../components/button/button.module";



@NgModule({
  declarations: [ProductListingComponent],
    imports: [
        CommonModule,
        CardModule,
        DropdownModule,
        SearchModule,
        ButtonModule
    ]
})
export class ProductListingModule { }
