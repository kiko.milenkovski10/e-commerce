import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {Product} from "../../models/product";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {AppState} from "../../state/app.state";
import {selectSearchValue} from "./selectors/search.selectors";
import {selectDropdownOption} from "./selectors/dropdown.selectors";
import {select, Store} from '@ngrx/store';
import * as DropdownActions from './actions/dropdown.actions';
import * as SearchActions from './actions/search.actions';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.component.html',
})
export class ProductListingComponent implements OnInit {
  products: Product[] = [];
  searchValue$: Observable<string>;
  dropdownOption$: Observable<string>;

  constructor(
    private readonly router: Router,
    private productService: ProductService,
    private readonly store: Store<AppState>) {
    this.searchValue$ = store.pipe(select(selectSearchValue));
    this.dropdownOption$ = store.pipe(select(selectDropdownOption));
  }

  ngOnInit(): void {
    this.searchValue$.subscribe((searchValue: string) => {
      this.dropdownOption$.subscribe((dropdownOption: string) => {
        // Call the getProducts function with the obtained string values
        this.productService.getProducts(dropdownOption, searchValue).subscribe((products) => {
          this.products = products;
        });
      });
    });
  }

  onDropdownChange(value: string): void {
    this.store.dispatch(DropdownActions.setDropdownOption({dropdownOption: value}));
  }

  search(value: string) {
    this.store.dispatch(SearchActions.setSearchValue({searchValue: value}));
  }

  goToShoppingCart() {
    this.router.navigateByUrl('/cart');
  }
}
