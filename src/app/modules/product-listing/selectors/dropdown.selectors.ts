import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DropdownState } from '../reducers/dropdown.reducer';

export const selectDropdownState = createFeatureSelector<DropdownState>('dropdown');

export const selectDropdownOption = createSelector(
  selectDropdownState,
  (state) => state.dropdownOption
);
