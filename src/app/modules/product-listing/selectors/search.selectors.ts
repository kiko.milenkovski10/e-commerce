import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SearchState } from '../reducers/search.reducer';

export const selectSearchState = createFeatureSelector<SearchState>('search');

export const selectSearchValue = createSelector(
  selectSearchState,
  (state) => state.searchValue
);
