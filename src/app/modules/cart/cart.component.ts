import {Component, OnInit} from '@angular/core';
import {CartService} from "../../services/cart.service";
import {Product, ProductVariant} from "../../models/product";
import {Router} from "@angular/router";


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
})
export class CartComponent implements OnInit {

  protected readonly Number = Number;
  products: Map<Product, Map<ProductVariant, number>> = new Map();

  constructor(private cartService: CartService, private readonly router: Router) {
  }

  ngOnInit(): void {
    this.remapProducts(this.cartService.getProducts());
  }

  remapProducts(products: Map<Product, ProductVariant[]>) {
    products.forEach((variantList, product) => {
      let map = new Map<ProductVariant, number>;
      variantList.forEach(variant => {
        let amount = map.get(variant);
        if (amount) {
          map.set(variant, amount + 1);
        } else {
          map.set(variant, 1);
        }
      })
      this.products.set(product, map);
    })
  }

  goToProducts() {
    this.router.navigateByUrl('/products')
  }

  calculateOrderTotal(tax: boolean) {
    let sum = 0;
    this.products.forEach(variantMap => {
      for (const [item, value] of variantMap.entries()) {
        if (tax) {
          sum += +item.priceWithTax * value
        } else {
          sum += +item.price * value
        }

      }
    })
    return sum;
  }

  checkOut() {
    alert("its done");
  }

  // does not work on GRAPHQL API for some reason
  // getActiveOrder(): void {
  //   this.apollo
  //     .query<ActiveOrderResponse>({
  //       query: this.cartService.getActiveOrder,
  //     })
  //     .subscribe((result) => {
  //       const activeOrder = result.data['activeOrder'];
  //       console.log('Active Order:', activeOrder);
  //     });
  // }
}
