import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CartComponent} from "./cart.component";
import {ImageModule} from "../../components/image/image.module";
import {ButtonModule} from "../../components/button/button.module";


@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    ImageModule,
    ButtonModule
  ]
})
export class CartModule {
}
