import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {Product, ProductVariant} from "../../models/product";
import {ActivatedRoute, Router} from "@angular/router";
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html'
})
export class ProductDetailsComponent implements OnInit {

  product!: Product;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
    private readonly router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const productId = params['id'];
      this.productService.getProductDetails(productId).subscribe((product) => {
        this.product = product;
      });
    })

  }

  addToCart(product: Product, variant: ProductVariant) {
    this.cartService.addProduct(product, variant);
  }

  goToShoppingCart() {
    this.router.navigateByUrl('/cart');
  }

  goToProducts(){
    this.router.navigateByUrl('/products')
  }
}

