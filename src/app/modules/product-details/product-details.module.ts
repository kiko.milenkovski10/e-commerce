import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductDetailsComponent} from "./product-details.component";
import {AppRoutingModule} from "../../app-routing.module";
import {ImageModule} from "../../components/image/image.module";
import {TextModule} from "../../components/text/text.module";
import {ButtonModule} from "../../components/button/button.module";
import {CardModule} from "../../components/card/card.module";
import {RouterModule} from "@angular/router";
import {Apollo} from "apollo-angular";

@NgModule({
  declarations: [ProductDetailsComponent],
  providers: [
    Apollo
  ],
  imports: [
    RouterModule.forRoot([]),
    CommonModule,
    AppRoutingModule,
    ImageModule,
    TextModule,
    ButtonModule,
    CardModule
  ]
})
export class ProductDetailsModule {
}
