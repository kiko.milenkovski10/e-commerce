import {Injectable} from '@angular/core';
import {Apollo} from "apollo-angular";
import {Product} from "../models/product";
import {map} from "rxjs";
import {QueryAllProducts, QueryProductDetails, QueryTotalProducts} from "../models/queries";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private apollo: Apollo) {
  }

  getTotalProducts() {
    return this.apollo
      .query<{ products: { totalItems: number } }>({query: QueryTotalProducts})
      .pipe(map((result) => result.data.products.totalItems));
  }

  getProducts(sort: string, filter: string) {
    return this.apollo
      .query<{ products: { items: Product[] } }>({query: QueryAllProducts, variables: {sort, filter}})
      .pipe(map((result) => result.data.products.items));
  }

  getProductDetails(id: string) {
    return this.apollo
      .query<{ product: Product }>({query: QueryProductDetails, variables: {id}})
      .pipe(map((result) => result.data.product));
  }

}
