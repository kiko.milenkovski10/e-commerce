import {Injectable} from '@angular/core';
import {Product, ProductVariant} from "../models/product";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private itemsInCart: Map<Product, ProductVariant[]> = new Map();

  constructor() {
  }

  addProduct(product: Product, variant: ProductVariant) {
    const existingVariants = this.itemsInCart.get(product);
    if (existingVariants) {
      existingVariants.push(variant);
      this.itemsInCart.set(product, existingVariants);
    } else {
      this.itemsInCart.set(product, [variant]);
    }
  }

  getProducts() {
    return this.itemsInCart;
  }

  // does not work on GRAPHQL API for some reason
  //   addProduct(id: string) {
  //     const mutation = gql`
  //       mutation addToOrder($id: ID!) {
  //         addItemToOrder(productVariantId: $id, quantity: 1){
  //               ... on Order{
  //             id
  //             total
  //            state
  //            code
  //                 totalWithTax
  //                 subTotal
  //           }
  //         }
  //       }
  // `;
  //
  //     return this.apollo
  //       .mutate<{ addItemToOrder: { code: string } }>({mutation, variables: {id}});
  //   }
  // getActiveOrder() {
  //   const query = gql`
  //   query {
  //     activeOrder {
  //       state
  //       id
  //       total
  //       totalWithTax
  //     }
  //   }
  // `;
  //
  //   console.log(query);
  //   return this.apollo.query<any>({query}).pipe(map((result) => {
  //     console.log("active order", result);
  //   }))
  // }
}
