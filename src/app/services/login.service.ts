import {Injectable} from '@angular/core';
import {Apollo} from "apollo-angular";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private apollo: Apollo) {
  }

//  does not work on GRAPHQL API for some reason
//   login() {
//     const mutation = gql`
//       mutation login {
//   login(username: "test@vendure.io", password: "test", rememberMe: true){
//   ... on CurrentUser{
//       id
//       identifier
//     }
//   ... on InvalidCredentialsError{
//       errorCode
//       authenticationError
//       message
//     }
//   ... on NotVerifiedError{
//       errorCode
//       message
//     }
//   }
// }`;
//     return this.apollo.mutate<any>({mutation})
//       .pipe(
//         map((result) => {
//           console.log("login call", result);
//           const loginResult = result.data?.login;
//           if (loginResult?.id) {
//             return loginResult.id;
//           } else if (loginResult?.errorCode) {
//             console.error(`Login error: ${loginResult.errorCode} - ${loginResult.message}`);
//             return null; // or throw an error or handle it as needed
//           } else {
//             console.error('Unexpected login result:', loginResult);
//             return null;
//           }
//         })
//       );
//   }
}
