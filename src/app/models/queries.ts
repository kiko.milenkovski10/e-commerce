import {gql} from "@apollo/client/core";

export const QueryTotalProducts = gql`
  query TotalProducts {
      products {
        totalItems
      }
     }
`;

// "take: 54" is hardcoded to avoid multiple calls. Use QueryTotalProducts to get the exact number
export const QueryAllProducts = gql`
      query Products($sort: SortOrder!, $filter: String) {
        products(options: { take: 54, sort: {name: $sort}, filter: {name: {contains: $filter}} }) {
          items {
            id
            name
            description
            featuredAsset {
              name
              source
            }
            variants {
              id
              name
              price
              priceWithTax
            }
          }
        }
      }
`;

export const QueryProductDetails = gql`
    query ProductDetails($id: ID!) {
      product(id: $id) {
            id
            name
            description
            featuredAsset {
              name
              source
            }
            variants {
              id
              name
              price
              priceWithTax
            }
          }
    }
  `;
