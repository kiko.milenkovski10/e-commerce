export interface Product {
  id: number;
  name: string;
  description: string;
  featuredAsset: Asset;
  variants: ProductVariant[];
}

export interface ProductVariant {
  id: string;
  productId: string;
  name: string;
  featuredAsset?: Asset;
  assets?: Asset[];
  price: SinglePrice;
  priceWithTax: SinglePrice;
  currencyCode: string;
  options: ProductOption[];
}

export interface SinglePrice {
  value: number;
}

export interface Asset {
  name: string;
  source: string
}

export interface ProductOption {
  id: string;
  name: string
}
