import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ProductListingModule} from "./modules/product-listing/product-listing.module";
import {GraphQLModule} from './graphql.module';
import {HttpClientModule} from '@angular/common/http';
import {ProductDetailsModule} from "./modules/product-details/product-details.module";
import {CartModule} from "./modules/cart/cart.module";
import { StoreModule } from '@ngrx/store';
import {searchReducer} from "./modules/product-listing/reducers/search.reducer";
import {dropdownReducer} from "./modules/product-listing/reducers/dropdown.reducer";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    StoreModule.forRoot({
      search: searchReducer,
      dropdown: dropdownReducer,
    }),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ProductListingModule,
    ProductDetailsModule,
    CartModule,
    GraphQLModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
