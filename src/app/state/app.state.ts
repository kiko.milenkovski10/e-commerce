import { SearchState } from '../modules/product-listing/reducers/search.reducer';
import { DropdownState } from '../modules/product-listing/reducers/dropdown.reducer';

export interface AppState {
  search: SearchState;
  dropdown: DropdownState;
}
