import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DropdownOption} from "./model/dropdown-option";
import {Store, select} from '@ngrx/store';
import {selectDropdownOption} from "../../modules/product-listing/selectors/dropdown.selectors";


@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html'
})
export class DropdownComponent implements OnInit {
  @Output("onDropdownChange") selectionChange: EventEmitter<string> = new EventEmitter<string>();
  selectedValue!: string;

  options: DropdownOption[] = [
    {
      label: "Name Ascending",
      value: "ASC"
    },
    {
      label: "Name Descending",
      value: "DESC"
    }
  ]

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    this.store.pipe(select(selectDropdownOption)).subscribe((value) => {
      this.selectedValue = value;
    });
  }

  onChange(): void {
    this.selectionChange.emit(this.selectedValue);
  }

}
