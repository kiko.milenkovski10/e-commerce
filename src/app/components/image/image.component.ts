import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html'
})
export class ImageComponent {
  @Input()
  src!: string | undefined;
}
