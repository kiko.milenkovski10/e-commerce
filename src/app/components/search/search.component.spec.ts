import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SearchComponent} from './search.component';
import {Apollo} from "apollo-angular";
import {
  ActionsSubject,
  ReducerManager,
  ReducerManagerDispatcher,
  StateObservable,
  Store,
  StoreModule
} from "@ngrx/store";
import {SearchModule} from "./search.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent],
      providers: [Apollo, Store, StateObservable, ActionsSubject, ReducerManager, ReducerManagerDispatcher],
      imports: [StoreModule.forRoot({}), SearchModule, BrowserAnimationsModule]

    });
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
