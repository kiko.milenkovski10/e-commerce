import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as SearchActions from '../../modules/product-listing/actions/search.actions';
import {selectSearchValue} from "../../modules/product-listing/selectors/search.selectors";
import {Store, select} from '@ngrx/store';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {
  searchValue: string = '';

  @Output() searchChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.pipe(select(selectSearchValue)).subscribe((value) => {
      this.searchValue = value;
    });
  }

  onInputChange(event: any) {
    const inputValue = event.target.value;
    this.store.dispatch(SearchActions.setSearchValue({searchValue: inputValue}));
    this.searchChanged.emit(inputValue);
  }
}
