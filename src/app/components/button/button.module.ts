import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from "./button.component";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [ButtonComponent],
  exports: [
    ButtonComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ]
})
export class ButtonModule {
}
