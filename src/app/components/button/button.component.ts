import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ThemePalette} from "@angular/material/core";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent {

  @Input()
  value!: string;

  @Input()
  styleType: 'none' | 'primary' | 'secondary' = 'none';

  @Input()
  color: ThemePalette = undefined;

  @Input()
  disabled: boolean = false;

  @Input()
  icon!: string;

  @Output('buttonClick')
  private clickEventEmitter: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  click(): void {
    this.clickEventEmitter.emit();
  }
}
