import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardComponent} from "./card.component";
import {TextModule} from "../text/text.module";
import {ImageModule} from "../image/image.module";
import {ButtonModule} from "../button/button.module";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [CardComponent],
  exports: [
    CardComponent
  ],
  imports: [
    ImageModule,
    CommonModule,
    TextModule,
    ImageModule,
    ButtonModule,
    RouterModule
  ]
})
export class CardModule {
}
