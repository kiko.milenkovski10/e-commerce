import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import {ImageModule} from "../image/image.module";
import {CommonModule} from "@angular/common";
import {TextModule} from "../text/text.module";
import {ButtonModule} from "../button/button.module";
import {RouterModule} from "@angular/router";
import {StoreModule} from "@ngrx/store";

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent],
      imports: [
        RouterModule.forRoot([]),
        CommonModule,
        TextModule,
        ImageModule,
        ButtonModule,
        RouterModule
      ]
    });
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
