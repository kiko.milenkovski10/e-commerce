import {Component, Input} from '@angular/core';
import {Product, ProductVariant} from "../../models/product";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html'
})
export class CardComponent {
  @Input()
  product!: Product | ProductVariant;

  constructor() {
  }
}
